package windowsbuilderejemplos;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;

public class WindowsMejorado {

	private JFrame frmCalculadora;
	private JFrame otroFrame;
	private JTextField txtNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
					WindowsMejorado window = new WindowsMejorado();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WindowsMejorado() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();

		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setIconImage(Toolkit.getDefaultToolkit().getImage(WindowsMejorado.class.getResource("/img/calculadora.png")));
		frmCalculadora.setBounds(100, 100, 650, 482);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Ingrese su nombre :");
		lblNewLabel.setBounds(11, 14, 120, 14);
		frmCalculadora.getContentPane().add(lblNewLabel);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(162, 11, 161, 20);
		frmCalculadora.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblIngreseSuClub = new JLabel("Ingrese su Club:");
		lblIngreseSuClub.setBounds(10, 53, 120, 14);
		frmCalculadora.getContentPane().add(lblIngreseSuClub);
		
		JComboBox comboBoxClubes = new JComboBox();
		comboBoxClubes.setBounds(162, 42, 161, 22);
		comboBoxClubes.setModel(new DefaultComboBoxModel (new 
				String[] 
						{"(Seleccione un Club)", "River Plate"
						, "Boca Juniors", "Independiente"
						, "Racing Club", "San Lorenzo"
						, "Otros"
						}
				));
		frmCalculadora.getContentPane().add(comboBoxClubes);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(178, 421, 85, 23);
		frmCalculadora.getContentPane().add(btnCancelar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.setBounds(10, 421, 85, 23);
		frmCalculadora.getContentPane().add(btnAceptar);
		
		JRadioButton rdbtnSoltero = new JRadioButton("Soltero");
		buttonGroup.add(rdbtnSoltero);
		JRadioButton rdbtnCasado =  new JRadioButton("Casado");
		buttonGroup.add(rdbtnCasado);

		rdbtnSoltero.setBounds(151, 83, 97, 23);
		rdbtnCasado.setBounds(151, 109, 97, 31);
		frmCalculadora.getContentPane().add(rdbtnSoltero);
		frmCalculadora.getContentPane().add(rdbtnCasado);
		
		JLabel lblIntereses = new JLabel("Intereses:");
		lblIntereses.setBounds(11, 151, 70, 14);
		frmCalculadora.getContentPane().add(lblIntereses);
		
		JCheckBox chckbxMsica = new JCheckBox("M\u00FAsica");
		chckbxMsica.setBounds(152, 147, 97, 23);
		frmCalculadora.getContentPane().add(chckbxMsica);
		
		JCheckBox chckbxArte = new JCheckBox("Arte");
		chckbxArte.setBounds(152, 171, 97, 23);
		frmCalculadora.getContentPane().add(chckbxArte);
		
		JCheckBox chckbxDeportes = new JCheckBox("Deportes");
		chckbxDeportes.setBounds(152, 197, 97, 23);
		frmCalculadora.getContentPane().add(chckbxDeportes);
		
		JLabel lblNewLabel_1 = new JLabel("Estado Civil:");
		lblNewLabel_1.setBounds(10, 87, 85, 14);
		frmCalculadora.getContentPane().add(lblNewLabel_1);
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nombre");
		model.addColumn("Domicilio");

		model.addRow(new String[] { "25145125", "Jos�", "Tribulato 332" });
		model.addRow(new String[] { "29434342", "Micaela", "Mitre 2265" });
		model.addRow(new String[] { "31525214", "Luciana", "Mitre 1730" });
		model.addRow(new String[] { "34425214", "Pedro", "Salta 330" });
	
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(11, 231, 403, 125);
		frmCalculadora.getContentPane().add(scrollPane_1);
		
		table = new JTable();
		scrollPane_1.setViewportView(table);
		
		table.setModel(model);
		
		DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
		dtcr.setHorizontalAlignment( JLabel.CENTER );
		dtcr.setBackground( Color.RED );
		dtcr.setForeground( Color.WHITE );
		
		table.setAutoResizeMode( JTable.AUTO_RESIZE_OFF);		
		table.getColumnModel().getColumn(0).setPreferredWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(150);
		table.getColumnModel().getColumn(2).setPreferredWidth(200);

		
		TableModel tm = table.getModel();
		System.out.println( tm.getValueAt(2, 2) );

		
		TableColumn col = table.getColumnModel().getColumn(0);
		col.setCellRenderer(dtcr);

		otroFrame = new JFrame();		
		otroFrame.setVisible(true);
		
	}
}
